import { StyleSheet, Dimensions } from 'react-native'
const { width, height } = Dimensions.get('window')
const screenWidth = width < height ? width : height

export default StyleSheet.create({
  day: {
    margin: 10,
    color: '#fff',
    fontSize: 18,
  },
  txtHeaderDate: {
    color: 'black',
    fontSize: 24,
    width: 116,
    textAlign: 'center'
  },
  weekdays: {
    margin: 10,
    color: 'white',
    width: screenWidth / 8,
    fontSize: 20,
    textAlign: 'center'
  },
  warpDay: {
    width: screenWidth / 8,
    height: screenWidth / 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icLockRed: {
    width: 13 / 2,
    height: 9,
    position: 'absolute',
    top: 2,
    left: 1
  },
  container:{
    marginHorizontal: 16
  },
  selectedDay:{
    height: screenWidth / 8,
    width: screenWidth / 8,
    backgroundColor: '#90C418',
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  aprovedDot:{
    position: 'absolute',
    width: 12,
    height: 12,
    borderRadius: 50,
    backgroundColor: '#90C418',
    right: 0,
    bottom: 0,
  },
  waitingDot:{
    position: 'absolute',
    width: 12,
    height: 12,
    borderRadius: 50,
    backgroundColor: '#F8C822',
    right: 0,
    bottom: 0,
  },
  rejectedDot:{
    position: 'absolute',
    width: 12,
    height: 12,
    borderRadius: 50,
    backgroundColor: '#FC4850',
    right: 0,
    bottom: 0,
  }
})
