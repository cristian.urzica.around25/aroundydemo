import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  FlatList,
  TouchableOpacity,
  Image
} from 'react-native'
import CommonFn from './commonFn'
import moment from 'moment'
import styles from './styles/CalendarSelectStyles'
import _ from 'lodash'
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { icons } from '../../../../src/assets'

class CalendarSelect extends Component {
  constructor(props) {
    super(props)
    this.state = {
      viewMode: 'day',
      selectedDates: []
    }
    this.getDotComponent = this.getDotComponent.bind(this)
  }

  getDotComponent(status, day) {
    if (status.aprovedDays.includes(day)) {
      return (
        <View style={styles.aprovedDot}></View>
      )
    }
    if (status.waitingDays.includes(day)) {
      return (
        <View style={styles.waitingDot}></View>
      )
    }
    if (status.rejectedDays.includes(day)) {
      return (
        <View style={styles.rejectedDot}></View>
      )
    }
  }

  renderDay(day) {
    const { calendarMonth, date, warpDayStyle, dateSelectedWarpDayStyle,
      renderChildDay, textDayStyle, currentDayStyle, notCurrentDayOfMonthStyle } = this.props
    const isCurrentMonth = calendarMonth === CommonFn.ym()
    const isCurrent = isCurrentMonth && CommonFn.ymd() === day
    const dateSelected = date && CommonFn.ymd(date) === day
    const notCurrentMonth = day.indexOf(calendarMonth) !== 0
    return (
      <TouchableOpacity onPress={() => this.props.onDatePress(day)}
        style={[{ backgroundColor: 'white', borderWidth: 0 }, styles.warpDay]}
      >
        <View style={this.props.dateStatus.selectedDays.includes(day) ? styles.selectedDay : { color: 'black' }}>
          {renderChildDay(day)}
          <Text style={[styles.day, textDayStyle, { color: 'black' },
          notCurrentMonth ? { color: 'grey', ...notCurrentDayOfMonthStyle } : {}]}>
            {day.split('-')[2]}
          </Text>
          {this.getDotComponent(this.props.dateStatus, day)}
        </View>
      </TouchableOpacity>
    )
  }

  selectDate(date) {
    if (this.isDateEnable(date)) {
      this.setState({
        ...this.state,
        selectedDates: [...this.state.selectedDates, date]
      })
      //this.props.selectDate(date)
    }
  }

  yearMonthChange(type, unit) {
    let { viewMode, currentYear } = this.state
    if (viewMode === 'day') {
      this.props.calendarChange(type, unit)
    } else {
      this.setState({
        currentYear: currentYear + (type < 0 ? -12 : 12)
      })
    }
  }

  isDateEnable(date) {
    const { minDate, maxDate } = this.props
    return date >= minDate && date <= maxDate
  }

  render() {
    const {
      calendarMonth, renderPrevYearButton, renderPrevMonthButton,
      renderNextYearButton, renderNextMonthButton,
      weekdayStyle, customWeekdays, warpRowWeekdays,
      warpRowControlMonthYear
    } = this.props
    const weekdays = customWeekdays || ['S', 'M', 'T', 'W', 'T', 'F', 'S']
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    const data = CommonFn.calendarArray(calendarMonth)
    var dayOfWeek = []
    _.forEach(weekdays, (element, index) => {
      dayOfWeek.push(<Text key={index} style={[styles.weekdays, weekdayStyle, { color: '#E77A39', margin: 5 }]}>{element}</Text>)
    })
    return (
      <View style={styles.container}>
        <View style={[{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 16 }, warpRowControlMonthYear]}>
          <TouchableOpacity style={{ marginRight: 8 }} onPress={() => this.yearMonthChange(-1, 'month')}>
            {renderPrevMonthButton ? renderPrevMonthButton() : <Image source={icons.arrowLeft} />}
          </TouchableOpacity>
          <Text style={styles.txtHeaderDate}>{months[parseInt(calendarMonth.split('-')[1], 10) - 1]}</Text>
          <TouchableOpacity style={{ marginLeft: 8 }} onPress={() => this.yearMonthChange(1, 'month')}>
            {renderNextYearButton ? renderNextYearButton() : <Image source={icons.arrowRight} />}
          </TouchableOpacity>
        </View>
        <FlatList
          data={data}
          keyExtractor={(item) => item}
          renderItem={({ item }) => this.renderDay(item)}
          extraData={this.state}
          numColumns={7}
        />
        <View style={[{ flexDirection: 'row', justifyContent: 'space-around' }, warpRowWeekdays]}>
          {dayOfWeek}
        </View>
      </View>
    )
  }
}

const propTypes = {
  customWeekdays: PropTypes.array,
  renderPrevYearButton: PropTypes.func,
  renderPrevMonthButton: PropTypes.func,
  renderNextYearButton: PropTypes.func,
  renderNextMonthButton: PropTypes.func,
  //style
  warpRowControlMonthYear: PropTypes.object,
  warpRowWeekdays: PropTypes.object,
  weekdayStyle: PropTypes.object,
  textDayStyle: PropTypes.object,
  currentDayStyle: PropTypes.object,
  notCurrentDayOfMonthStyle: PropTypes.object,
  warpDayStyle: PropTypes.object,
  dateSelectedWarpDayStyle: PropTypes.object,

}

const defaultProps = {
  customWeekdays: ['S', 'M', 'T', 'W', 'T', 'F', 'S']
}

CalendarSelect.propTypes = propTypes
CalendarSelect.defaultProps = defaultProps
export default CalendarSelect;