const images = {
    background: require('./images/background/bck.png'),
    employeeBg1: require('./images/employee-balance-bg1/bg1.png'),
    thumbnail: require('./images/flyAwayThumbnail/thumb.png'),
    profileThumbnail: require('./images/profileThumbnail/thumbnail.png'),
    profilePic: require('./images/profile-pic/profile.png'),
    loading: require('./images/loading/loading.gif'),
    logo: require('./images/logo/logo.png')
}

const icons = {
    locked: require('./icons/locked/locked.png'),
    user: require('./icons/user/user.png'),
    europeActive: require('./icons/europe/europe-active.png'),
    europeInactive: require('./icons/europe/europe-inactive.png'),
    rocketActive: require('./icons/rocket/rocket-active.png'),
    rocketInactive: require('./icons/rocket/rocket-inactive.png'),
    helmActive: require('./icons/helm/helm-active.png'),
    helmInactive: require('./icons/helm/helm-inactive.png'),
    download: require('./icons/download/download.png'),
    arrowLeft: require('./icons/arrow-left/arrow-left.png'),
    arrowRight: require('./icons/arrow-rigth/arrow-right.png'),
    balance: require('./icons/balance/balance.png'),
    questionCloud: require('./icons/question-cloud/question-cloud.png'),
    checkmark: require('./icons/checkmark/checkmark.png'),
    checkmarkus: require('./icons/checkmark-us/checkmark-us.png'),
    aproved: require('./icons/aproved/aproved.png'),
    denied: require('./icons/denied/denied.png'),
    pending: require('./icons/pending/pending.png')
}

export {
    images,
    icons
}