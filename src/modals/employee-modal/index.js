import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'

import styles from './style'

export default class EmployeeModal extends Component {
    render() {
        return (
            <View style={[styles.base, this.props.style]}>
                <TouchableOpacity 
                style={styles.button}
                onPress={this.props.onFirstBPress}>
                    <Text style={styles.text}>{this.props.firstBText}</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                style={styles.button}
                onPress={this.props.onSecondBPress}>
                    <Text style={styles.text}>{this.props.secondBText}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}
