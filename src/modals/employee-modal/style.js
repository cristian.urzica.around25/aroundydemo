import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
    base:{
        flexDirection:'row',
        justifyContent: 'space-between',
        paddingHorizontal: '10%',
        alignItems: 'flex-end',
        paddingBottom: '7%',

    },
    button:{
        width: '46%',
        height: '10%',
        paddingVertical: 25,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        backgroundColor: 'white',
        elevation: 10
    },
    text:{
        fontSize: 20
    }
})

export default styles