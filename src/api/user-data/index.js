import { create } from 'apisauce'
import CacheService from '../../services/cache'

testToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZmlyc3ROYW1lIjoiQWRtaW4iLCJsYXN0TmFtZSI6IklzdHJhdG9yIiwicm9sZXMiOlsiZW1wbG95ZWUiLCJwcm9qZWN0LW1hbmFnZXIiLCJodW1hbi1yZXNvdXJjZXMiLCJtYW5hZ2VyIiwiYWRtaW5pc3RyYXRvciJdLCJyZXF1ZXN0SWQiOiI1MTkyMzBjNy0wODI2LTQ1OWYtOGU3NC1kMTMwZWU4OTEwYzAiLCJpYXQiOjE1NjYyOTA4MjksImV4cCI6MTU2Njg5NTYyOX0.Lnn2grUmnwejBiFQguEOHOEb99bozEYRzatvnsg7IgY'

const initApi = (token) => {
    userDataApi = create({
        baseURL: 'https://aroundy-02.democlient.info/auth/current-user',
        headers: {
            'authorization': token
        }
    })
}

let userDataApi;

const getUserData = async (token) => {
    let response = await userDataApi.get(
)
    return response.data.data
}

export {
    initApi,
    getUserData
}