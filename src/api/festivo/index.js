import { create } from 'apisauce'
import CacheService from '../../services/cache'

const key = "d049bc61-fe38-48b2-9c8f-d980daa6a72b"

const festivoApi = create({
    baseURL: 'https://getfestivo.com/v1/holidays'
})

//CAlls

const getFestivoHolidays = async(year, country='RO') => {
    //Verifica cache
    let cachedResponse = await CacheService.get(CacheService.BANK_HOLIDAYS)
    if (cachedResponse){
         return cachedResponse
     }
    let response =  await festivoApi.get(
        '?api_key='+key+
        '&country='+country+
        '&year='+year
    )
    await CacheService.set(CacheService.BANK_HOLIDAYS, response.data.holidays.holidays, CacheService.TTL_ONE_DAY)
    return response.data.holidays.holidays
}

export { getFestivoHolidays }