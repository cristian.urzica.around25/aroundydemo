import { create } from 'apisauce'
import CacheService from '../../services/cache'

const loginApi = create({
  baseURL: 'https://aroundy-02.democlient.info'
})

//Calls
const postLogin = async (email, password) => {
  // let cachedUser = await CacheService.get(CacheService.USER_TOKEN)
  // let cachedRefresh = await CacheService.get(CacheService.REFRESH_TOKEN)
  // if (cachedUser && cachedRefresh) {
  //   console.log("GOT FROM CACHE")
  //   return {
  //     isCached: true,
  //     data: {
  //       acces_token: cachedUser,
  //       refresh_token: cachedRefresh
  //     }
  //   }
  // }
  let response = await loginApi.post(
    '/auth/login',
    {
      email: email,
      password: password
    }
  )
  if (!response.ok) {
    return {
      data: null
    }
  }
  await CacheService.set(CacheService.USER_TOKEN, response.data.data.access_token)
  await CacheService.set(CacheService.REFRESH_TOKEN, response.data.data.refresh_token)

  return {
    isCached: false,
    data: {
      acces_token: response.data.data.access_token,
      refresh_token: response.data.data.refresh_token
    }
  }
}

export {
  postLogin
}