import { create } from 'apisauce'


let requestApi = create({
  baseURL: 'https://aroundy-02.democlient.info/request',
  headers: {
    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwib3JnYW5pemF0aW9uX2lkIjoxLCJmaXJzdE5hbWUiOiJBZG1pbiIsImxhc3ROYW1lIjoiSXN0cmF0b3IiLCJyb2xlcyI6WyJlbXBsb3llZSIsInByb2plY3QtbWFuYWdlciIsImh1bWFuLXJlc291cmNlcyIsIm1hbmFnZXIiLCJhZG1pbmlzdHJhdG9yIl0sInJlcXVlc3RJZCI6ImJkZGI0NDY4LWVhYTctNDFmNy1iYzM0LWFjMmU1ZGYwMTM4NSIsImlhdCI6MTU2NzA2NDg5MCwiZXhwIjoxNTY3NjY5NjkwfQ.9_dqBVlhLlDuETqah5ekKP4hgkuZK_vgg-X6iecZrWk'
  }
})

const setAprovalToken = (token) => {
  requestApi = create({
    baseURL: 'https://aroundy-02.democlient.info/request',
    headers: {
      'Authorization': 'Bearer ' + token
    }
  })
}

const getAprovalRequests = async () => {
  try {
    let response = await requestApi.get()
    return response
  }
  catch(err){
    return null
  }
}

const postAprovalRequest = async(id, status) => {
    let response = requestApi.put('/approval/' + id, { status: status })
}

export {
  getAprovalRequests,
  postAprovalRequest,
  setAprovalToken
}