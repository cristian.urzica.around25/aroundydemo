const H_REQUESTS = [
  {
    from: 1566259200000,
    to: 1567123200000,
    period: 10,
    type: null,
    status: 'rejected',
    created_at: null,
    updated_at: null,
    user_id: null
  },
  {
    from: 1565222400000,
    to: 1565568000000,
    period: 10,
    type: null,
    status: 'waiting',
    created_at: null,
    updated_at: null,
    user_id: null
  },
]

const H_BOSSES = [
  {
    name:'Cristina Ilies',
  },
  {
    name:'Florina Vasile',
  },
  {
    name:'Paul Chirila',
  },
  {
    name:'Adrian Boros',
  },
  {
    name:'Boss test1',
  },
  {
    name:'Boss test2'
  },
  {
    name:'Boss test3',
  }
]

export {
  H_REQUESTS,
  H_BOSSES
}