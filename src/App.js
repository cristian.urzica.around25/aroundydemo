/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import ChoseLeave from './screens/chose-leave'
import store from './redux/store'
import { Provider } from 'react-redux'

import Root from './modules/Root'

const App = () => {
  return (
    <Provider store={store}>
      <Root/>
    </Provider>
  );
};

export default App;
