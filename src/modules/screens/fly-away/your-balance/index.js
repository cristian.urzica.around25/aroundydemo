import React, { Component } from 'react'
import { Text, View, ImageBackground } from 'react-native'

import { icons } from '../../../../assets'
import styles from './style'
import OrangeDates from '../../../../components/orange-dates-scroll-viewer'
import NextBackModal from '../../../../modals/employee-modal'
import Navigator from '../../../../services/navigation'
import IconTextValue from '../../../../components/icon-text-value'
import { calculateRequestedDays } from '../../../../utility'
import Header from '../../../../components/page-header'

export default class YourBalance extends Component {

  constructor(props) {
    console.disableYellowBox = true;
    super(props);
    this.state = {
    };
    this.onBackPressHandler = this.onBackPressHandler.bind(this)
    this.onProceedPressHandler = this.onProceedPressHandler.bind(this)
  }

  onBackPressHandler() {
    if (this.props.singleDays.length > 0) {
      Navigator.goTo("HalfDays")
      return
    }
    Navigator.goTo("LeaveType")
  }

  onProceedPressHandler() {
    Navigator.goTo('SendBosses')
  }

  render() {
    return (
      <View>
        <Header title="Your balance" />

        <OrangeDates
          style={{ height: '10%', marginTop: 64 }}
          intervalsDays={this.props.intervalsDays}
          singleDays={this.props.singleDays} />

        <View style={{ marginTop: 64, paddingHorizontal: '10%' }}>

          <IconTextValue description={'Balance'}
            value={this.props.userData.Balance.remaining}
            icon={icons.balance} />
          <IconTextValue
            description={'Requested'}
            value={calculateRequestedDays(this.props.holidaysData, this.props.employeeChoicesSelectedDates, this.props.singleDays, this.props.halfDays)}
            icon={icons.questionCloud} />
        </View>
        <View style={{ flex: 1 }}>
          <NextBackModal
            firstBText='BACK'
            secondBText='PROCEED'
            onFirstBPress={this.onBackPressHandler}
            onSecondBPress={this.onProceedPressHandler}
            style={{ marginTop: 64 }} />
        </View>
      </View>
    )
  }
}
