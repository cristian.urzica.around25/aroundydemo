import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
    base: {
        flex:1,
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    thumbnail: {
        overflow: 'hidden',
        width: width,
        height: height / 6,
        borderBottomLeftRadius: 60,
        borderBottomRightRadius: 60,
        justifyContent: 'center',
      },
      headerTitle: {
        fontSize: 28,
        color: 'white',
        marginHorizontal: 25
      },           
      balanceContainer:{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-start',
          marginVertical: 8
      },
      normalText:{
          fontSize: 20,
          
      },
      valueText:{
          fontSize: 28,
          width: '50%',
          textAlign: 'center',
          flex:1
      }
})

export default styles