import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import YourBalance from './index';

const mapStateToProps = (state) => {
    return ({
        employeeChoicesSelectedDates: state.employeeChoices.selectedDates,
        intervalsDays: state.employeeChoices.intervalsDays,
        singleDays: state.employeeChoices.singleDays,
        halfDays: state.employeeChoices.halfDays,
        userData: state.tokensReducer.userData,
        holidaysData: state.bankHoliday.bankHolidays,
    })
}
const mapDispatchToProps = (dispatch) => bindActionCreators({
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(YourBalance)