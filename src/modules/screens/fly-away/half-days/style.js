import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
  base: {
    flex: 1
  },
  thumbnail: {
    overflow: 'hidden',
    width: width,
    height: height / 6,
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
    justifyContent: 'center',
  },
  headerTitle: {
    fontSize: 28,
    color: 'white',
    marginHorizontal: 25
  },
  list:{
    height: '65%',
    marginHorizontal: 32,
  },
  orangeDates:{
    height: '20%', 
    marginVertical: 32 
  },
  nextBack:{
    height: '17%',
    flexDirection: 'column',
  },
  listDescriptor:{
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    marginHorizontal: 32,
    marginBottom: 16
  },
  listDescription:{
    fontSize: 22,
    color: '#FF9123'
  }
})

export default styles