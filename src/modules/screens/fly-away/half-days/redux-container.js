import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import HalfDays from './index'
import { putHalfDays } from '../../../../redux/employee-choices/actions'

const mapStateToProps = (state) => {
  return ({
    employeeChoicesSelectedDates: state.employeeChoices.selectedDates,
    intervalsDays: state.employeeChoices.intervalsDays,
    singleDays: state.employeeChoices.singleDays

  })
}
const mapDispatchToProps = (dispatch) => bindActionCreators({
  putHalfDays: putHalfDays
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(HalfDays)