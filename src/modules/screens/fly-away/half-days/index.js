import React, { Component } from 'react'
import { Text, View, ImageBackground, FlatList, Switch } from 'react-native'

import OrangeDates from '../../../../components/orange-dates-scroll-viewer'
import styles from './style'
import { images } from '../../../../assets'
import NextBackModal from '../../../../modals/employee-modal'
import { fromateDateDayMonth } from '../../../../utility'
import HalfComponent from '../../../../components/half-days-list-element'
import Navigator from '../../../../services/navigation'
import Header from '../../../../components/page-header'

export default class HalfDays extends Component {

  constructor(props) {
    console.disableYellowBox = true;
    super(props);
    this.state = {
      halfDaysStatus: this.props.singleDays.map(element => false)
    };
    this.getListComponent = this.getListComponent.bind(this)
    this.switchOnChange = this.switchOnChange.bind(this)
    this.onBackButtonPress = this.onBackButtonPress.bind(this)
    this.onProceedButtonPress = this.onProceedButtonPress.bind(this)
  }

  switchOnChange(index) {
    console.log(index)
    let newHalfDaysStatus = [...this.state.halfDaysStatus]
    newHalfDaysStatus[index] = !newHalfDaysStatus[index]
    console.log("New half: ", newHalfDaysStatus)
    this.setState({
      ...this.state,
      halfDaysStatus: [...newHalfDaysStatus]
    })
  }

  getListComponent(date) {
    const day = new Date(date)
    return (
      <HalfComponent
        index={date.index}
        state={this.state.halfDaysStatus[date.index]}
        date={date}
        key={date.index}
        callback={(index) => this.switchOnChange(index)} />)
  }

  onProceedButtonPress() {
    this.props.putHalfDays(this.state.halfDaysStatus)
    Navigator.goTo("YourBalance")
  }

  onBackButtonPress() {
    Navigator.goTo("LeaveType")
  }

  render() {
    return (
      <View style={styles.base}>

        <Header title="Half days" />

        <OrangeDates
          style={styles.orangeDates}
          intervalsDays={this.props.intervalsDays}
          singleDays={this.props.singleDays} />

        <View style={styles.listDescriptor}>
          <Text style={styles.listDescription}>Date</Text>
          <Text style={styles.listDescription}>Half day</Text>
        </View>

        <FlatList
          style={styles.list}
          showsVerticalScrollIndicator={true}
          data={this.props.singleDays}
          renderItem={(element) => this.getListComponent(element)} />

        <View style={styles.nextBack}>
          <NextBackModal
            firstBText='BACK'
            secondBText='PROCEED'
            onFirstBPress={this.onBackButtonPress}
            onSecondBPress={this.onProceedButtonPress} />
        </View>
      </View>
    )

  }
}
