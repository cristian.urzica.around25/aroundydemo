import React, { Component } from 'react'
import { Text, View, TextInput, FlatList, ScrollView } from 'react-native'

import Header from '../../../../components/page-header'
import OrangeDates from '../../../../components/orange-dates-scroll-viewer'
import NextBackModal from '../../../../components/next-back-modal'

import styles from './style'
import Boss from '../../../../components/boss-element'
import { H_BOSSES } from '../../../../hardcodamcuspor'
import Navigator from '../../../../services/navigation'
import { postRequest } from '../../../../api/holiday'
import Loading from '../../../../components/loading'
import { formateDateForApi } from '../../../../utility'

//TEXT_INPUT_EVENT + BUTTONS_LISTENERS
export default class SendBosses extends Component {

  constructor(props) {
    console.disableYellowBox = true;
    super(props);
    this.state = {
      bossesStates: [],
      comment: '',
      loading: false
    }

    this.getListElement = this.getListElement.bind(this)
    this.clickOnComponentHandler = this.clickOnComponentHandler.bind(this)
    this.backButtonClickEvent = this.backButtonClickEvent.bind(this)
    this.onCommentChange = this.onCommentChange.bind(this)
    this.setMeFreeButtonEvent = this.setMeFreeButtonEvent.bind(this)
  }

  getListElement(name, index) {
    console.log("enter here : ", name, ', ', index)
    return (
      <Boss name={name} index={index} callback={this.clickOnComponentHandler} />
    )
  }

  componentDidMount() {
    this.setState({
      bossesStates: H_BOSSES.map(element => false)
    })
  }

  clickOnComponentHandler(index) {
    console.log("Index: ", index)
    let newBossesStates = [...this.state.bossesStates]
    newBossesStates[index] = !newBossesStates[index]
    console.log('New state: ', newBossesStates)
    this.setState({
      ...this.state,
      bossesStates: [...newBossesStates]
    })
  }

  onCommentChange(text){
    this.setState({
      ...this.state,
      comment: text
    })
  }

  backButtonClickEvent(){
    Navigator.goTo("YourBalance")
  }

  async setMeFreeButtonEvent(){
    //WORK FOR HERE
    this.setState({
      ...this.state,
      loading: true
    })
    for(i = 0; i < this.props.intervalsDays.length; i+=2){
      const dateS = formateDateForApi(this.props.intervalsDays[i])
      const dateF = formateDateForApi(this.props.intervalsDays[i+1])
      await postRequest(
        dateS,
        dateF,
        false,
        this.props.selectedType,
        [1],
        this.props.comment,
      )
    }

    this.props.singleDays.map( async (date, index)=>{
      const dateSF = formateDateForApi(date)
      await postRequest(
        dateSF,
        dateSF,
        this.props.halfDays[index],
        this.props.selectedType,
        [1],
        this.props.comment,
      )
    })
    
    this.setState({
      ...this.state,
      loading: false
    })
    Navigator.goTo("FlyAwayCalendar")
  }

  render() {
    if(this.state.loading){
      return <Loading/>
    }
    return (
      <View style={{height: 900}}>
        <Header title="Send to bosses" />
        <View style={styles.base}>
          <OrangeDates
            intervalsDays={this.props.intervalsDays}
            singleDays={this.props.singleDays}
            style={styles.orangeDates} />
          <TextInput
            placeholder='Comment'
            underlineColorAndroid='grey'
            style={styles.inputText}
            onChangeText={(value) => this.onCommentChange(value)} />

          <FlatList
            style={styles.list}
            data={H_BOSSES}
            renderItem={(item) => { return (this.getListElement(item.item.name, H_BOSSES.indexOf(item.item))) }}
          />
        </View>

        <View style={styles.nextBack}>
            <NextBackModal
              firstButtonText='BACK'
              secondButtonText='SET ME FREE'
              onFirstButtonPress={this.backButtonClickEvent}
              onSecondButtonPress={this.setMeFreeButtonEvent}
              isOrange={true}/>
          </View>
      </View>
    )
  }
}
