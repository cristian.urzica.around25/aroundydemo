import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import YourBalance from './index';

const mapStateToProps = (state) => {
  return ({
    intervalsDays: state.employeeChoices.intervalsDays,
    singleDays: state.employeeChoices.singleDays,
    halfDays: state.employeeChoices.halfDays,
    selectedType: state.employeeChoices.selectedType,
    userData: state.tokensReducer.userData
  })
}
const mapDispatchToProps = (dispatch) => bindActionCreators({
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(YourBalance)