import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
  base: {
    paddingHorizontal: '10%',
    paddingTop: 32,
  },
  inputText:{
    fontSize: 22,
    marginTop: 16
  },
  list:{
    marginTop: 16,
    height: '25%'
  },
  nextBack:{
    height: '17%',
    flexDirection: 'column',
    alignSelf: 'center',
    marginTop: 32
  },
  orangeDates:{
    height: '8%',
  }
})

export default styles