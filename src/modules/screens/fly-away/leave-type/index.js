import React, { Component } from 'react'
import { Text, View, ImageBackground, ScrollView } from 'react-native'

import styles from './style'
import { images } from '../../../../assets'
import { isConsecutiveDay } from '../../../../utility'
import { WheelPicker } from 'react-native-wheel-picker-android'
import NextBackModal from '../../../../modals/employee-modal'
import OrangeDates from '../../../../components/orange-dates-scroll-viewer'
import Navigator from '../../../../services/navigation'
import Header from '../../../../components/page-header'

const wheelPickerData = ['Anual', 'Medical', 'Sick day', 'Fam. emergency', 'Blood donation', 'Time of in lieu', 'Delegative', 'Breavement', 'Weeding', 'Payless', 'Paternal', 'Training']
const holiTypes = ['annual', 'medical', 'sick-day', 'emergency', 'blood-donation', 'compensed-free-time', 'delegation', 'bereavement', 'marriage', 'no-payment', 'paternal']

export default class LeaveType extends Component {

  constructor(props) {
    console.disableYellowBox = true;
    super(props);
    this.state = {
      selectedItem: 1,
      intervalsDays: [],
      singleDays: []
    };
    this.getHolidayPeriods = this.getHolidayPeriods.bind(this)
    this.onWheelElementChangeEventHandler = this.onWheelElementChangeEventHandler.bind(this)
    this.onProceedButtonPerssEventHandler = this.onProceedButtonPerssEventHandler.bind(this)
    this.onBackButtonPressEventHandler = this.onBackButtonPressEventHandler.bind(this)
  }

  getHolidayPeriods() {
    intervalsDays = []
    singleDays = []
    indexStart = 0
    indexEnd = 0
    findEndFlah = false
    for (i = 0; i < this.props.employeeChoicesSelectedDates.length; i++) {
      if (isConsecutiveDay(this.props.employeeChoicesSelectedDates[i], this.props.employeeChoicesSelectedDates[i + 1])) {
        indexEnd++
      }
      else {
        if (indexEnd === indexStart) {
          singleDays = [...singleDays, this.props.employeeChoicesSelectedDates[indexStart]]
        }
        else {
          intervalsDays = [...intervalsDays, this.props.employeeChoicesSelectedDates[indexStart], this.props.employeeChoicesSelectedDates[indexEnd]]
        }
        indexStart = i + 1
        indexEnd = i + 1
      }
    }
    this.setState({
      ...this.state,
      intervalsDays,
      singleDays
    })
  }

  onWheelElementChangeEventHandler(selectedItem) {
    this.setState({
      selectedItem
    })
  }

  onProceedButtonPerssEventHandler() {
    this.props.putSingleDays(this.state.singleDays)
    this.props.putIntervalsDays(this.state.intervalsDays)
    this.props.putSelectedType(holiTypes[this.state.selectedItem])
    if (this.state.singleDays.length === 0) {
      Navigator.goTo('YourBalance')
      return
    }
    Navigator.goTo('HalfDays')
  }

  onBackButtonPressEventHandler() {
    Navigator.goTo("FlyAwayCalendar")
  }

  componentDidMount() {
    this.getHolidayPeriods()
  }

  render() {
    return (
      <View>
        <Header title="Leave type" />

        <View style={styles.contentContainer}>
          <OrangeDates
            style={{ height: '40%' }}
            intervalsDays={this.state.intervalsDays}
            singleDays={this.state.singleDays} />
          <View style={styles.wheelPicker}>
            <WheelPicker
              selectedItem={this.state.selectedItem}
              data={wheelPickerData}
              onItemSelected={this.onWheelElementChangeEventHandler}
              itemTextSize={28}
              selectedItemTextSize={28}
              indicatorColor='white' />
          </View>
        </View>

        <View style={{ flex: 1 }}>
          <NextBackModal
            firstBText='BACK'
            secondBText='PROCEED'
            onFirstBPress={this.onBackButtonPressEventHandler}
            onSecondBPress={this.onProceedButtonPerssEventHandler}
            style={{ marginTop: 64 }} />
        </View>


      </View>
    )
  }
}
