import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
    thumbnail: {
        overflow: 'hidden',
        width: width,
        height: height / 6,
        borderBottomLeftRadius: 60,
        borderBottomRightRadius: 60,
        justifyContent: 'center',
      },
      headerTitle: {
        fontSize: 28,
        color: 'white',
        marginHorizontal: 25
      },
      contentContainer:{
        marginStart: 32,
        marginEnd: 32,
        marginTop: 32,
        marginBottom: 16,
        height: height / 2,
        alignItems:'center',
        paddingTop: 16,
      },
      orangeText:{
        fontSize: 22,
        color: '#FF9123',
        marginVertical: 2
      },
      wheelPicker:{
        marginTop: 32,
        alignItems: 'center',
        height: '60%',
      }
})

export default styles