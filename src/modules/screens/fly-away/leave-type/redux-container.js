import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { putSelectedType,
         putIntervalsDays,
         putSingleDays } from '../../../../redux/employee-choices/actions'

import LeaveType from './index'

const mapStateToProps = (state) => {
    return ({
        employeeChoicesSelectedDates: state.employeeChoices.selectedDates,
        holidaysData: state.bankHoliday.bankHolidays
    })
}
const mapDispatchToProps = (dispatch) => bindActionCreators({
        putSelectedType: putSelectedType,
        putIntervalsDays,
        putSingleDays
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(LeaveType)