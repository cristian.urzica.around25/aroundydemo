import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import FlyAway from './index'
import { putSelectedDates } from '../../../../redux/employee-choices/actions'
import {
    postUserData
} from '../../../../redux/user-data/actions'

const mapStateToProps = (state) => {
    return({
        employeeChoicesSelectedDates: state.employeeChoices.selectedDates,
        userData: state.tokensReducer.userData,
        userToken: state.tokensReducer.userToken,
    })
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    employeePutSelectedDates: putSelectedDates,
    postUserData: (data) => postUserData(data)
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(FlyAway)