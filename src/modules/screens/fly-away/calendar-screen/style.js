import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
  thumbnail: {
    overflow: 'hidden',
    width: width,
    height: height / 6,
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
    justifyContent: 'center'
  },
  headerTitle: {
    fontSize: 35,
    color: 'white',
    marginHorizontal: 25
  },
  container: {
    flex: 1,
    // padding: 5
  },
  icLockRed: {
    width: 13 / 2,
    height: 9,
    position: 'absolute',
    right: 2,
    bottom: 1
  },
  legendContainer: {
    marginTop: 8,
    flexDirection: 'row',
    alignItems: 'center',
    width: width,
    height: 50,
    justifyContent: 'center'
  },
  miniContainer:{
    marginHorizontal: 8,
    flexDirection: 'row',
    alignItems: 'center'
  },
  dot: {
    height: 26,
    width: 26,
    marginRight: 8,
    borderRadius: 50
  },
  nextBackModal:{
    marginTop: 54,
    marginHorizontal: '10%'
  },
  download:{
    position: "absolute",
    height: 35,
    width: 35,
    right: '9%',
    top: height/4.8,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon:{
    width: 24,
    height: 24
  }
})

export default styles