import React, { Component } from 'react';
import { View, Text, ImageBackground, Image, TouchableOpacity } from 'react-native';
import DateTime from 'react-native-customize-selected-date'

import { images, icons } from '../../../../assets'
import styles from './style'
import NextBackModal from '../../../../components/next-back-modal'
import Navigator from '../../../../services/navigation'
import Header from '../../../../components/page-header'
import { getUserData } from '../../../../api/user-data'
import Loading from '../../../../components/loading'

const mokedHolidayDays = ['8-11-2019', '8-12-2019', '8-13-2019', '8-14-2019', '8-22-2019', '8-22-2019', '8-11-2019', '8-12-2019', '8-13-2019', '8-14-2019', '8-11-2019', '8-12-2019', '8-13-2019', '8-14-2019', '8-22-2019', '8-22-2019', '8-11-2019', '8-12-2019', '8-13-2019', '8-14-2019']

function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
}

export default class FlyAway extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDays: [],
      unselectedDays: [],
      aprovedDays: [],
      waitingDays: [],
      rejectedDays: [],
      loading: true
    };

    this.selectDay = this.selectDay.bind(this)
    this.getDateStatus = this.selectDay.bind(this)
    this.checkForIntervals = this.checkForIntervals.bind(this)
    this.getModal = this.getModal.bind(this)
    this.onCancelClickEventHander = this.onCancelClickEventHander.bind(this)
    this.onProceedClickEventHandler = this.onProceedClickEventHandler.bind(this)
    this.initDaysWithStatus = this.initDaysWithStatus.bind(this)
    this.onDownloadHandler = this.onDownloadHandler.bind(this)
  }

  //Functionality
  initDaysWithStatus() {
    newAprovedDays = []
    newRejectedDays = []
    newWiatingDays = [] 
    console.log(this.props.userData.Requests.length)
    this.props.userData.Requests.map((element) => {
      console.log(element)
      const from = new Date(element.from).getTime()
      const to = new Date(element.to).getTime()
      for (date = from; date <= to; date += 86400000) {
        switch (element.status) {
          case ('processing'):
            newWiatingDays = [...newWiatingDays, formatDate(date)]
            break

          case ('pending'):
            newWiatingDays = [...newWiatingDays, formatDate(date)]
            break

          case ('aproved'):
            newAprovedDays = [...newAprovedDays, formatDate(date)]
            break

          case ('not-approved'):
            newRejectedDays = [...newRejectedDays, formatDate(date)]
            break
        }
      }
    })
    this.setState({
      ...this.state,
      waitingDays: newWiatingDays,
      rejectedDays: newRejectedDays,
      aprovedDays: newAprovedDays,
      loading: false
    })
  }

  getDateStatus() {
    return {
      selectedDays,
      unselectedDays,
      aprovedDays,
      waitingDays,
      rejectedDays
    }
  }

  deleteElementFromList(element, list) {
    return list.filter(item => item != element).sort(
      (a, b) => { return (new Date(a).getTime() - (new Date(b).getTime())) })
  }

  addElementInList(element, list) {
    if (list.includes(element)) return list
    return [...list, element].sort(
      (a, b) => { return (new Date(a).getTime() - (new Date(b).getTime())) })
  }

  sele
  ctDay(day) {
    if (!this.state.selectedDays.includes(day)) {
      this.setState({
        ...this.state,
        selectedDays: this.addElementInList(day, this.state.selectedDays)
      })
    }
    else {
      this.setState({
        ...this.state,
        selectedDays: this.deleteElementFromList(day, this.state.selectedDays),
        unselectedDays: this.addElementInList(day, this.state.unselectedDays)
      })
    }
  }

  checkForIntervals() {
    const isConsecutiveDay = (day1, day2) => {
      const d1 = new Date(day1).getTime()
      const d2 = new Date(day2).getTime()
      if ((d1 - d2 === 86400000) || (d2 - d1 === 86400000)) {
        return true
      }
      return false
    }
    function formatDate(date) {
      var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return [year, month, day].join('-');
    }

    let newSelectedDays = [...this.state.selectedDays]

    for (let index = 0; index < newSelectedDays.length - 1; index++) {
      if (newSelectedDays[index + 1]) {
        if (!isConsecutiveDay(newSelectedDays[index], newSelectedDays[index + 1])) {
          dateStart = new Date(newSelectedDays[index])
          dateEnd = new Date(newSelectedDays[index + 1])
          dateInner = []
          for (date = dateStart.getTime() + 86400000; date < dateEnd.getTime(); date = date + 86400000) {
            const newDate = new Date(date)
            if (!this.state.unselectedDays.includes(formatDate(newDate))) {
              dateInner.push(formatDate(newDate))
            }
          }
          if (dateInner.length !== 0) {
            newSelectedDays = [...newSelectedDays.slice(0, index + 1),
            ...dateInner,
            ...newSelectedDays.slice(index + 1, newSelectedDays.length)]
            this.setState({
              ...this.state,
              selectedDays: newSelectedDays
            })
            break
          }
        }
      }
    }
  }

  componentDidUpdate() {
    this.checkForIntervals()
    Navigator.setRef(this.props.navigation)
  }

  //Events handlers
  onCancelClickEventHander() {
    console.log("Call this")
    this.setState({
      ...this.state,
      selectedDays: [],
      unselectedDays: []
    })
  }

  onProceedClickEventHandler() {
    console.log("Click on proceed")
    this.props.employeePutSelectedDates(this.state.selectedDays)
    //this.props.employeePutSelectedDates(mokedHolidayDays) /// ASTA STEARSA
    Navigator.goTo('LeaveType')
  }

  //UI
  renderChildDay(day) {
    return null
  }

  getLegendComponent(color, text) {
    return (
      <View style={[styles.miniContainer]}>
        <View style={[styles.dot, { backgroundColor: color }]} />
        <Text>{text}</Text>
      </View>
    )
  }

  async componentDidMount() {
    this.initDaysWithStatus()
  }

  getModal() {
    if (this.state.selectedDays.length === 0) return null

    return (
      <NextBackModal
        style={[styles.nextBackModal]}
        firstButtonText="CANCEL"
        secondButtonText="PROCEED"
        onFirstButtonPress={this.onCancelClickEventHander}
        onSecondButtonPress={this.onProceedClickEventHandler} />
    )
  }

  async onDownloadHandler(){
    this.setState({
      ...this.state,
      loading: true
    })
    const response = await getUserData(this.props.userToken)
    this.props.postUserData(response)
    this.initDaysWithStatus()
    this.setState({
      ...this.state,
      loading: false
    })
  }

  render() {
    if(this.state.loading){
      return(<Loading/>)
    }
    return (
      <View style={{ flex: 1 }}>
        <Header title="Fly Away" />
        <View>
          <DateTime
            date={this.state.time}
            changeDate={(date) => this.onChangeDate(date)}
            format='YYYY-MM-DD'
            renderChildDay={(day) => this.renderChildDay(day)}
            onDatePress={this.selectDay}
            dateStatus={this.state}
          />
        </View>
        {this.getModal()}
        <View style={this.state.selectedDays.length > 0 ? styles.legendContainer : [styles.legendContainer, { marginTop: 64 }]}>
          {this.getLegendComponent('#90C418', 'Aproved')}
          {this.getLegendComponent('#F8C822', 'Waiting')}
          {this.getLegendComponent('#FC4850', 'Rejected')}
        </View>
        <TouchableOpacity style={styles.download} onPress={this.onDownloadHandler}>
          <Image source={icons.download} style={styles.icon} />
        </TouchableOpacity>
      </View>
    );
  }
}

