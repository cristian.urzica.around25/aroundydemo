import React, { Component } from 'react'
import { Text, View, ImageBackground, ScrollView, ActivityIndicator } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

import { images } from '../../../../assets'
import styles from './style'
import Request from '../../../../components/request'
import { getAprovalRequests, postAprovalRequest } from '../../../../api/aproval-request'

export default class AdminAproval extends Component {

  constructor(props) {
    console.disableYellowBox = true;
    super(props);
    this.state = {
      weeks: 0,
      days: 0,
      hours: 0,
      minutes: 0,
      seconds: 0,
      upcommingHolidays: [],
      requests: [],
      flag: true
    };
    this.upcommingHolidays = []
    this.getTimeTillChristhmas = this.getTimeTillChristhmas.bind(this)
    this.getBankHoliComponent = this.getBankHoliComponent.bind(this)
    this.callbackClickHandler = this.callbackClickHandler.bind(this)
  }

  getTimeTillChristhmas(){
    if(this.upcommingHolidays.length === 0) return
    let date = new Date()
    let christhmasDate = new Date(this.upcommingHolidays[0].duration)
    let delta = christhmasDate - date.getTime();
    let seconds= Math.trunc(delta/1000)
    let minutes = Math.trunc(seconds / 60)
    seconds = seconds % 60
    let hours = Math.trunc(minutes/60)
    minutes = minutes % 60
    let days = Math.trunc(hours/24)
    hours = hours % 24
    let weeks = Math.trunc(days / 7)
    days = days % 7
    this.setState({
      ...this.state,
      weeks,
      days,
      hours,
      minutes,
      seconds,
    })
  }

  async componentDidMount(){
    this.getTimeTillChristhmas()
    setInterval(this.getTimeTillChristhmas, 1000)
    await this.props.getHolidays()
    const response = await getAprovalRequests()
    console.log("Response from requests: ",response)
    this.setState({
      ...this.state,
      requests: response.data.data
    })
  }

  getBankHoliComponent(holidayName, duration, index){
  const today = new Date()
  if(today.getTime() > new Date(duration).getTime()) return null
  this.upcommingHolidays = [...this.upcommingHolidays, {name: holidayName, duration}]
    return(
      null
    )
  }

  callbackClickHandler(state, index){
    if(state === 'aprove'){
      postAprovalRequest(this.state.requests[index].id, 'approved')
      let newRequests = [...this.state.requests]
      newRequests[index].status = 'approved'
      this.setState({
        ...this.state,
        requests: [...newRequests]
      })
      return
    }
    postAprovalRequest(this.state.requests[index].id, 'not-approved')
    let newRequests = [...this.state.requests]
    newRequests[index].status = 'not-approved'
    this.setState({
      ...this.state,
      requests: [...newRequests]
    })
  }

  getRequestComponent(element, index){
    if(element.status !== 'pending') {
      return null
    }
    return(
      <Request 
        index={index}
        callback={this.callbackClickHandler}
        name={element.name}
        from={element.from}
        to={element.to}
        />
    )
  }
  
  render() {
    if(this.props.holidaysAPI.pending){
      return(
        <ActivityIndicator style={{flex:1}}/>
      )
    }
    return (
      <ScrollView style={{flex:1}}>
        <ImageBackground
          style={styles.topImage} 
          source={images.employeeBg1}>
          <Text style={styles.untilChristhmas}>{this.state.weeks+'w '+this.state.days+'d '+this.state.hours+'h '+ this.state.minutes+"m "+ this.state.seconds + 's '}</Text>
            <Text style={styles.smallGreyText}>until {this.upcommingHolidays.length > 0 ? this.upcommingHolidays[0].name : ''}</Text>
        </ImageBackground>
        <LinearGradient
          style={styles.linearGradient}
          colors={['#F84100', '#F9D213']}>
          <View style={styles.balanceContainer}>
            <Text style={styles.textBigWhite}>Balance</Text>
            <Text style={[styles.textBigWhite,{marginStart: '30%'}]}> {this.props.userData.Balance.remaining} </Text>
            <Text style={styles.textSmallWhite}> Days </Text>
          </View>
        </LinearGradient>
        <View style={styles.bankHolidays}>
          <Text style={styles.bankText}>Bank Holidays</Text>
          <View style={styles.holidayDescriptionTable}>
          </View>
          { this.props.holidaysData.map((holiday , index) => {
              return this.getBankHoliComponent(holiday.name, holiday.date, index)
          })}
          {this.state.requests.map((element, index) => {
            return this.getRequestComponent(element, index)
          })}
        </View>
      </ScrollView>
    )
  }
}
