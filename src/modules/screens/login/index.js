import React, { Component } from 'react'
import { Text, View, ImageBackground, TouchableOpacity, Image } from 'react-native'

import { images, icons } from '../../../assets'
import styles from './style'
import Loading from '../../../components/loading'
import CustomUserInput from '../../../components/input-text-user'
import GradientButton from '../../../components/orange-button'
import Navigation from '../../../services/navigation'
import { postLogin } from '../../../api/login'
import { getUserData, initApi } from '../../../api/user-data';
import { H_REQUESTS } from '../../../hardcodamcuspor'
import { setRequestToken } from '../../../api/requests'
import { setHolidayToken } from '../../../api/holiday'
import { setAprovalToken } from '../../../api/aproval-request'


export default class Login extends Component {

  constructor(props) {
    console.disableYellowBox = true;
    super(props);
    this.state = {
      username: '',
      password: '',
      loading: false
    };

    this.updateField = this.updateField.bind(this)
    this.onLoginPressEventHandler = this.onLoginPressEventHandler.bind(this)
    this.getNextScreenByRole = this.getNextScreenByRole.bind(this)
  }

  updateField(field, value){
    this.setState({
      ...this.state,
      [field]: value
    }
    )
  }

  async onLoginPressEventHandler(){
    this.setState({
      ...this.state,
      loading: true
    })
    const response = await postLogin(
      this.state.username,
      this.state.password)
    if(!response.data){
      return
    }
    this.props.postUserToken(response.data.acces_token)
    this.props.postRefreshToken(response.data.refresh_token)
    this.getNextScreenByRole = this.getNextScreenByRole.bind(this)
    setRequestToken(response.data.acces_token)
    setHolidayToken(response.data.acces_token)
    setAprovalToken(response.data.acces_token)
    await initApi(this.props.userToken)
    const userData = await getUserData(this.props.userToken)
    userData.Balance = {
      id: 1,
      total: 30,
      remaining: 20,
      locked: 2,
      nextYear: 30
    }
    this.props.postUserData(userData)
    // Check function here
    //Navigation.goTo('Employee')
      Navigation.goTo(this.getNextScreenByRole(userData.Roles))
    this.setState({
      ...this.state,
      loading: false
    })
  }

  onForgotPasswordEventHandler(){
  }

  getNextScreenByRole(roles){
    //Check user role and chose a screen flow here
    let admin = false
    roles.forEach((item) => {
      if(item.name === 'administrator'){
        admin = true
      }
    })
    if(admin){
      return("Admin")
    }
    return("Employee")
  }

  async componentDidMount(){
    this.setState({
      ...this.state,
      loading: true
    })
    Navigation.setRef(this.props.navigation)
    // Check cached data about tokens
    const response = await postLogin(null, null)
    if(!response.data){
      this.setState({
        ...this.state,
        loading: false
      })
      return
    }
    this.props.postUserToken(response.data.acces_token)
    this.props.postRefreshToken(response.data.refresh_token)
    setRequestToken(response.data.acces_token)
    setHolidayToken(response.data.acces_token)
    setAprovalToken(response.data.acces_token)
    await initApi(this.props.userToken)
    if(response.isCached){
      const userData = await getUserData(this.props.userToken)
      //userData.Requests = H_REQUESTS //ASTA ZOABARA DE AICI
      userData.Balance = {
        id: 1,
        total: 30,
        remaining: 20,
        locked: 2,
        nextYear: 30
      }
      this.props.postUserData(userData)
      //Navigation.goTo('Employee')
      Navigation.goTo(this.getNextScreenByRole(userData.Roles))
      this.setState({
        ...this.state,
        loading: false
      })
      // check role function here
    }
  }

  render() {
    if(this.state.loading){
      return <Loading/>
    }
    return (
      <View style={{flex:1}}>
        <ImageBackground
          source={images.background}
          style={styles.base}>
          <Image style={styles.logo} source={images.logo}/>
          <CustomUserInput
            style={styles.input}
            width='80%'
            icon={icons.user}
            placeholder="Username"
            onTextChange={(value) => this.updateField("username", value)} />
          <CustomUserInput
            style={styles.input}
            width='80%'
            icon={icons.locked}
            placeholder="Password"
            onTextChange={(value) => this.updateField("password", value)}
            isPassword={true}  />

          <TouchableOpacity style={styles.forgotContainer}>
            <Text style={styles.forgot}>Forgot password</Text>
          </TouchableOpacity>

          <GradientButton
          style={styles.loginButton}
          text={'LOGIN'}
          onPress={this.onLoginPressEventHandler}/>
        </ImageBackground>
      </View>
    )
  }
}
