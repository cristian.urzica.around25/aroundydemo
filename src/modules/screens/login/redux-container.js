import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Login from './index'
import {
  postUserToken,
  postRefreshToken,
  postUserData
} from '../../../redux/user-data/actions'

const mapStateToProps = (state) => {
    return({
        userToken: state.tokensReducer.userToken,
        refreshToken: state.tokensReducer.refreshToken,
        userData: state.tokensReducer.userData
    })
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    postUserToken: (token) => postUserToken(token),
    postRefreshToken: (token) => postRefreshToken(token),
    postUserData: (data) => postUserData(data)
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Login)