import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';
import { whileStatement } from '@babel/types';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
    linearGradient:{
        height: height / 2,
        borderRadius: 60,
        marginTop: -50
    },
    bankHolidays:{
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        marginTop: -200,
        backgroundColor: 'white',
        borderColor: 'black',
        // height: '100%',
        paddingBottom:32,
        width: '100%',
        paddingHorizontal: '10%'
    },
    textBigWhite:{
        fontSize: 30,
        color: 'white'
    },
    textSmallWhite:{
        fontSize: 18,
        color: 'white'
    },
    balanceContainer:{
        paddingHorizontal: '10%',
        flexDirection: 'row',
        width: '100%',
        height: height/7,
        justifyContent: "space-around",
        alignItems: 'flex-end',
        paddingTop: height/15
    },
    topImage:{
        width:'100%',
        height:height / 3.5,
    },
    untilChristhmas:{
        fontSize: 30,
        color: 'white',
        alignSelf: 'center',
        marginTop: height / 14
    },
    smallGreyText:{
        fontSize: 14,
        color: '#878787',
        alignSelf: 'center'
    },
    bankText:{
        fontSize: 30,
        marginTop: 38,
        color:'grey'
    },
    holidayDescriptionTable:{
        marginTop: 32,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    orangeText:{
        color:'#FF7534'
    },
    bankHolidayContainer:{
        marginTop: 16,
        paddingVertical: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: 'grey'
    }
})

export default styles