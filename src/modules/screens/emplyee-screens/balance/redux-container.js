import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import EmployeeBalance from './index'
import getBankHolidaysFromApi from '../../../../redux/bank-holidays/actions'

const mapStateToProps = (state) => {
    return({
        holidaysData: state.bankHoliday.bankHolidays,
        holidaysAPI: state.bankHoliday.api,
        userToken: state.tokensReducer.userToken,
        refreshToken: state.tokensReducer.refreshToken,
        userData: state.tokensReducer.userData
    })
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    getHolidays: getBankHolidaysFromApi
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeBalance)