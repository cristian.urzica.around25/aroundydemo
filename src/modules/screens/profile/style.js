import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
	base: {
		width: '100%',
		height: '100%',
		justifyContent: 'center',
		alignItems: 'center'
	},

	thumbnailContainer:{
		width: width,
		height: height/3,
	},

	thumbnail: {
		width: width,
		height: height / 4,
		borderBottomLeftRadius: 60,
		borderBottomRightRadius: 60,
		paddingHorizontal: '10%',
		alignItems: 'center',
		flexDirection: 'row'
	},
	profileInfo: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	profilePic: {
		borderRadius: 100,
		marginBottom: -150
	},
	title: {
		color: 'white',
		fontSize: 24,
		marginStart: 16,
		marginTop: 100,
	},
	requestContainer:{
		height: height / 4,
		paddingHorizontal: '10%',
		justifyContent: 'flex-start',
		paddingTop: 8
	},
	normalText:{
		fontSize: 20,
		alignSelf: 'center',
		color: '#646464'
	},
	orangeDates:{
		marginVertical: 8,
		height: height/10
	},
	cancelText:{
		fontSize: 18,
		color: '#ff9123',
		alignSelf: 'center',
		marginVertical: 8
	},
	requestButton:{
		height: 60,
		width: '90%',
		alignSelf: 'center',
		marginTop: 8
	},
	historyContainer:{
    marginTop: 32,
    borderTopEndRadius: 62,
    borderTopLeftRadius: 62,
    paddingHorizontal: '10%',
    paddingVertical: 48,
    elevation: 6
  },
  categoryTitle:{
    fontSize: 30,
  },
  description:{
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    marginVertical: 8
  },
  descriptionText:{
    color: '#FF8C42'
  },
  historyElementContainer:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 4
  },
  historyText:{
    fontSize: 14,
    textAlign: 'left',
    flex: 1
  },
  statusIcon:{
    marginRight: 6
  }
})

export default styles