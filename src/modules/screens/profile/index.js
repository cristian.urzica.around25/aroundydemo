import React, { Component } from 'react'
import { Text, View, ScrollView, ImageBackground, Image } from 'react-native'

import { images, icons } from '../../../assets'
import styles from './style';
import { getRequests } from '../../../api/requests'
import OrangeDates from '../../../components/orange-dates-scroll-viewer'
import { TouchableOpacity } from 'react-native-gesture-handler';
import GradientButton from '../../../components/orange-button'
import { generateDaysBetween, formateDateForApi } from '../../../utility'
import Navigator from '../../../services/navigation'

export default class Profile extends Component {

  constructor(props) {
    console.disableYellowBox = true;
    super(props);
    this.state = {
      loading: true,
      requests: [],
      intervalsDays: [],
      singleDays: [],
      allDays: []
    };
    this.setDaysArrays = this.setDaysArrays.bind(this)
    this.getRequestComponent = this.getRequestComponent.bind(this)
    this.getHistoryComponent = this.getHistoryComponent.bind(this)
    this.requestADayHandler = this.requestADayHandler.bind(this)
  }

  setDaysArrays(response){
    let newIntervalsDays = []
    let newSingleDays = []
    let newAllDays = []

    response.data.data.map(element => {
      if (element.from !== element.to) {
        newIntervalsDays = [element.from.slice(0, 10), element.to.slice(0, 10), ...newIntervalsDays]
        newAllDays = [...newAllDays, 
          ...generateDaysBetween(element.from.slice(0, 10), element.to.slice(0,10), element.status, element.type)]
        return
      }
      newSingleDays = [element.from.slice(0, 10), ...newSingleDays]
      newAllDays = [ element.from.slice(0,10), ...newAllDays ]
    })

    this.setState({
      ...this.state,
      intervalsDays: [...newIntervalsDays],
      singleDays: [...newSingleDays],
      allDays: [...newAllDays]
    })
  }

  async componentDidMount() {
    Navigator.setRef(this.props.navigation)
    const response = await getRequests()
    this.setState({
      ...this.state,
      requests: [...response.data.data]
    })
    this.setDaysArrays(response)
  }

  getRequestComponent() {
    if (this.state.requests.length === 0) {
      return
    }
    return (
      <View style={styles.requestContainer}>
        <Text style={styles.normalText}>
          You requested
      </Text>
        <OrangeDates
          intervalsDays={this.state.intervalsDays}
          singleDays={this.state.singleDays}
          style={styles.orangeDates} />
        <Text style={styles.normalText}>
          of Vacation.
      </Text>
        <Text style={[styles.normalText, { color: 'black' }]}>
          Fingers crossed!
      </Text>
      </View>)
  }

  getHistoryComponent(element, index){
    if(!element.status){
      return null
    }
    function getIcon(status){
      if(status === 'pending' || status === 'processing'){
        return(icons.pending)
      }
      if(status === 'not-approved'){
        return(icons.denied)
      }
      return(icons.aproved)
    }
    return(
      <View key={index} style={styles.historyElementContainer}>
        <Text style={[styles.historyText]}>{formateDateForApi(element.date)}</Text>
        <Text style={[styles.historyText]}>{element.type}</Text>
        <Image style={styles.statusIcon} source={getIcon(element.status)}></Image>
      </View>
    )
  }

  requestADayHandler(){
    Navigator.goTo('FlyAway')
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.thumbnailContainer}>
          <ImageBackground
            source={images.profileThumbnail}
            style={styles.thumbnail}>
            <Image
              source={images.profilePic}
              style={styles.profilePic} />
            <Text style={styles.title}>Name</Text>
          </ImageBackground>
        </View>
        {this.getRequestComponent()}
        <TouchableOpacity>
          <Text style={styles.cancelText}>Cancel request</Text>
        </TouchableOpacity>
        <GradientButton
          text='REQUEST A DAY OFF'
          style={styles.requestButton}
          onPress={this.requestADayHandler}/>
        <View style={styles.historyContainer}>
          <Text style={styles.categoryTitle}>History</Text>
          <View style={styles.description}>
            <Text style={styles.descriptionText}>Date</Text>
            <Text style={styles.descriptionText}>Type</Text>
            <Text style={styles.descriptionText}>Status</Text>
          </View>
          {this.state.allDays.map((element, index) => this.getHistoryComponent(element, index))}
        </View>
      </ScrollView>
    )
  }
}
