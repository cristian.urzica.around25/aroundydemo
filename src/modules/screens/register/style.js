import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
    base: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo:{
        color:'white',
        fontSize: 18
    },
    input:{
        width: "80%",
        height: 50,
        marginStart: 8,
        marginTop: 36
    },
    loginButton:{
        width: 222,
        height: 56,
        marginTop: 36
    }
})

export default styles