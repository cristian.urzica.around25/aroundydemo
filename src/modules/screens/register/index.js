import React, { Component } from 'react'
import { Text, View, ImageBackground, TouchableOpacity } from 'react-native'

import { images, icons } from '../../../assets'
import styles from './style'
import CustomUserInput from '../../../components/input-text-user'
import GradientButton from '../../../components/orange-button'

export default class Register extends Component {

  constructor(props) {
    console.disableYellowBox = true;
    super(props);
    this.state = {
      password1: '',
      password2: ''
    };

    this.updateField = this.updateField.bind(this)
    this.validatePasswordTyping = this.updateField.bind(this)
  }

  updateField(field, value){
    this.setState({
      ...this.state,
      [field]: value
    }
    )
  }

  validatePasswordTyping(){
    return this.state.password1 === this.state.password2
  }


  onRegisterPressEventHandler(){
  }

  render() {
    return (
      <View>
        <ImageBackground
          source={images.background}
          style={styles.base}>
          <Text style={styles.logo}> LOGO </Text>
          <CustomUserInput
            style={styles.input}
            width='80%'
            icon={icons.locked}
            placeholder="Password"
            onTextChange={(value) => this.updateField("password1", value)}
            isPassword={true}  />
          
          <CustomUserInput
            style={styles.input}
            width='80%'
            icon={icons.locked}
            placeholder="Rewrite password"
            onTextChange={(value) => this.updateField("password2", value)}
            isPassword={true}  />

          <GradientButton
          style={styles.loginButton}
          text={"SET PASSWORD"}/>
        </ImageBackground>
      </View>
    )
  }
}
