import React, { Component } from 'react'
import { Text, View } from 'react-native'
import BaseAppContainer from '../navigator'

export default class Root extends Component {

  constructor(props) {
    console.disableYellowBox = true;
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <BaseAppContainer/>
      </View>
    )
  }
}
