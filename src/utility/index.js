const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

const formatDate = (date) => {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
}

const formateDateDayMonth = (date) => {
  var d = new Date(date),
    month = d.getMonth(),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;
  month = months[month]
  return [day, month].join('.');
}

const formateDateForApi = (date) => {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [day, month, year].join('-');
}

const isConsecutiveDay = (day1, day2) => {
  const d1 = new Date(day1).getTime()
  const d2 = new Date(day2).getTime()
  if ((d1 - d2 === 86400000) || (d2 - d1 === 86400000)) {
    return true
  }
  return false
}

const calculateRequestedDays = (holidays, selectedDays, singleDays, halfDays) => {

  let requestedBalance = 0
  const holidaysDays = holidays.map(element => element.date)
  if (!selectedDays) return
  selectedDays.map((element, index) => {
    day = new Date(element)
    if (day.getDay() === 0 ||
      day.getDay() === 6 ||
      singleDays.includes(element) ||
      holidaysDays.includes(element)) { return }
    requestedBalance += 1
  })

  if (singleDays) {
    singleDays.map((element, index) => {
      if (day.getDay() === 0 ||
        day.getDay() === 6 ||
        holidaysDays.includes(element)) { return }
      if (halfDays[index]) {
        requestedBalance += 0.5
        return
      }
      requestedBalance += 1
    })
  }
  return requestedBalance
}

const generateDaysBetween = (day1, day2, status, type) => {
  const dateS = new Date(day1)
  const dateF = new Date(day2)
  let days = []
  for (date = dateS.getTime(); date <= dateF.getTime(); date += 86400000){
    days = [...days, {
      date: formatDate(date),
      status: status,
      type: type
    }]
  } 

  return days
}

export {
  formatDate,
  formateDateDayMonth,
  isConsecutiveDay,
  calculateRequestedDays,
  formateDateForApi,
  generateDaysBetween
}