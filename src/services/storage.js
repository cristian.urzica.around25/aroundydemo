import { AsyncStorage } from 'react-native'

class Storage{
    static async set(key, value){
        if(!value){
            return
        }
        valueJson = JSON.stringify(value);
        await AsyncStorage.setItem(key,valueJson);
    }

    static async get(key){
        const result = await AsyncStorage.getItem(key);
        if(!result){
            return null
        }
        return JSON.parse(result)
    }

    static async del(key){
        return AsyncStorage.removeItem(key);
    }
}

Storage.NAVIGATION_STATE = 'nav_state';

export default Storage
