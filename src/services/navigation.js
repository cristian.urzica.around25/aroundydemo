import { StackActions, NavigationActions } from 'react-navigation'; 

const navigationSettings = (type, params) =>{
    if (type === "generalReset"){
    generalReset= StackActions.reset( {
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Welcome' , params})],
    })
    return generalReset
    }
    doneReset= StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Tab' , params})],
    })
    return doneReset
}

class Navigation {
    static navigation = null;

    static setRef(ref = {}) {
        this.navigation = ref
    }

    static setScreens(ref = {}) {
        this.screens = ref
    }

    static goTo(screen, params = {}) {
        this.navigation.navigate(screen, params)
    }

    static goToDispatch(screen, params = {}) {
        this.navigation.dispatch(navigationSettings(screen, params))
    }

    static goBack(){
        console.log("GO BACK")
        this.navigation.goBack()
    }
}

export default Navigation