import Storage from './storage'

export default class CacheService {
  static async set(key = '', data = {}, ttl = null) {
    if (ttl) {
      const ttlUpdated = (ttl * 1000) + new Date().valueOf();
      return Storage.set(key, { data, ttl: ttlUpdated })
    }
    Storage.set(key, {data, ttl: ttl})
  }

  static async get(key = '') {
    try {
      let response = await Storage.get(key)
      let today = new Date()
      if (!response) {
      }
      if (response.ttl) {
        if (today.getTime() >= response.ttl) {
        }
      }
      return response.data
    } catch (err) {
      console.log('Eroor: ', err)
      return null;
    }
  }

  static clear(key = '') {
    Storage.del(key)
  }
}

CacheService.BANK_HOLIDAYS = 'holythanks'
CacheService.USER_TOKEN = 'usertoken',
  CacheService.REFRESH_TOKEN = 'refreshtoken',
  CacheService.TTL_ONE_DAY = 86000
