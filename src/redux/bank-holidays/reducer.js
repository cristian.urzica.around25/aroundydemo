import types from './types'

const INITIAL_STATE = {
    bankHolidays: [],
    api: {
        pendind: false,
        succes: false,
        error: false
    }
}

const setApiState = (field='', value=false) => {
    return({
        ...INITIAL_STATE.api,
        [field]: value
    })
}

const bankHolidaysReducer = (state = INITIAL_STATE, action) =>{
    switch (action.type){
        case(types.getError):
            return {
                ...state,
                api: setApiState("error", true)
            }

        case(types.getPending):
            return {
                ...state,
                api: setApiState("pending", true)
            }

        case(types.getSucces):
            if(!action.payload){
                return{
                    ...state,
                    bankHolidays: [],
                    api: setApiState("succes", true)
                }
            }
            return {
                ...state,
                bankHolidays: action.payload,
                api: setApiState("succes", true)
            }

        default:
            return state
    }
}

export default bankHolidaysReducer