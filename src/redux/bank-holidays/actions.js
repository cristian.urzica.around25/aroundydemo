import holidaysActions from './action-creators'
import { getFestivoHolidays } from '../../api/festivo'

const getBankHolidaysFromApi = () => async (dispatch, getState) => {
    try {
      dispatch(holidaysActions.getPending())
      let today = new Date()
      response = await getFestivoHolidays(today.getFullYear(), 'RO')
      if (!response){
        throw new Error("No response from api...")
      }
      return dispatch(holidaysActions.getSucces(response))
    }
    catch(err){
      console.log("ERRRROOOORRRRRR: ", err)
      dispatch(holidaysActions.getError)
    }
}

export default getBankHolidaysFromApi