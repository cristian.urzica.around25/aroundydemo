import types from './types'

const getSucces = (bankHolidays) => {
    return{
        type: types.getSucces,
        payload: bankHolidays
    }
}

const getPending = () => {
    return{
        type: types.getPending
    }
}

const getError = () => {
    return {
        type: types.getError
    }
}

const holidaysActions = {
    getSucces,
    getError,
    getPending
}

export default holidaysActions