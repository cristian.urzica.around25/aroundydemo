export default types = {
    getSucces: 'GET/SUCCES/BANK_HOLIDAYS',
    getError: 'GET/ERROR/BANK_HOLIDAYS',
    getPending: 'GET/PENDING/BANK_HOLIDAYS'
}