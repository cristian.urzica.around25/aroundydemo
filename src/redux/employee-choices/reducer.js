import types from './types'

const INITIAL_STATE = {
  selectedDates: [],
  intervalsDays: [],
  singleDays: [],
  halfDays: [],
  selectedType: []
}

const employeeChoicesReducer = (state = INITIAL_STATE, action) => {
  console.log("Action: ", action)
  switch (action.type) {
    case (types.putSelectedDates):
      console.log("Heer, ",state)
      
      return {
        ...state,
        selectedDates: action.payload
      }

    case (types.putSelectedType):
      console.log("Here ai am")
      return {
        ...state,
        selectedType: action.payload
      }

    case (types.putIntervalsDays):
      console.log("Put intervals days")
      return{
        ...state,
        intervalsDays: action.payload
      }

    case (types.putSinglesDays):
      console.log("Put single days")
      return {
        ...state,
        singleDays: action.payload
      }

    case (types.putHalfDays):
      console.log("Put half days")
      return {
        ...state,
        halfDays: action.payload
      }
    default:
      return state
  }
}

export default employeeChoicesReducer