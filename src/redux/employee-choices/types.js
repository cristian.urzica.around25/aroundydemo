export default types = {
    putSelectedDates: 'PUT/SELECTED_DATES',
    putSelectedType: 'PUT/TYPE',
    putIntervalsDays: 'PUT/INTERVALS_DAYS',
    putSinglesDays: 'PUT/SINGLES_DAYS',
    putHalfDays: 'PUT/HALF_DAYS',
}