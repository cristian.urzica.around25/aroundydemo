import types from './types'

const putSelectedDates = (dates) => {
    return{
        type: types.putSelectedDates,
        payload: dates
    }
}

const putSelectedType = (typeSelected) => {
    return{
        type: types.putSelectedType,
        payload: typeSelected
    }
}

const putIntervalsDays = (data) => {
    return {
        type: types.putIntervalsDays,
        payload: data
    }
}

const putSingleDays = (data) => {
    return {
        type: types.putSinglesDays,
        payload: data
    }
}

const putHalfDays = (data) => {
    return{
        type: types.putHalfDays,
        payload: data
    }
}

const selectedDatesActions = {
    putSelectedDates,
    putSelectedType,
    putIntervalsDays,
    putSingleDays,
    putHalfDays,
}

export default selectedDatesActions