import selectedDatesActions from './action-creators'

const putSelectedDates = (dates) => (dispatch, getState) => {
  return dispatch(selectedDatesActions.putSelectedDates(dates))
}

const putSelectedType = (type) => (dispatch, getState) => {
  return dispatch(selectedDatesActions.putSelectedType(type))
}

const putIntervalsDays = (days) => (dispatch, getState) => {
  return dispatch(selectedDatesActions.putIntervalsDays(days))
}

const putSingleDays = (days) => (dispatch, getState) => {
  return dispatch(selectedDatesActions.putSingleDays(days))
}

const putHalfDays = (days) => (dispatch, getState) => {
  return dispatch(selectedDatesActions.putHalfDays(days))
}


export { putSelectedDates,
         putSelectedType,
         putIntervalsDays,
         putSingleDays,
         putHalfDays }
