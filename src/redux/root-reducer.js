import { combineReducers } from 'redux'
import bankHolidaysReducer from './bank-holidays/reducer'
import userTokensReducer from './user-data/reducer'
import employeeChoicesReducer from './employee-choices/reducer'

export default combineReducers({
    bankHoliday: bankHolidaysReducer,
    tokensReducer: userTokensReducer,
    employeeChoices: employeeChoicesReducer
})