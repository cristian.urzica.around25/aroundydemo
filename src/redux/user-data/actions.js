import userDataActions from './action-creators'

const postUserToken = (userToken) => (dispatch, getState) => {
  dispatch(userDataActions.postUserToken(userToken))
}

const postRefreshToken = (refreshToken) => (dispatch, getState) => {
  dispatch(userDataActions.postRefreshToken(refreshToken))
}

const postUserData = (userData) => (dispatch, getState) => {
  dispatch(userDataActions.postUserData(userData))
}

export {
  postUserToken,
  postRefreshToken,
  postUserData
}