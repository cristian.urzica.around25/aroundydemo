export default types = {
    postUserToken: 'POST/USER_TOKEN',
    postRefreshToken: 'POST/REFRESH_TOKEN',
    postUserData: 'POST/USER_DATA'
}