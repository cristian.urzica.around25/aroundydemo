import types from './types'

const INITIAL_STATE = {
    userToken: null,
    refreshToken: null,
    userData: null
}

const userTokensReducer = (state = INITIAL_STATE, action) =>{
    switch (action.type){
        case(types.postUserToken):
            return {
                ...state,
                userToken: action.payload
            }
        
        case(types.postRefreshToken):
            return {
                ...state,
                refreshToken: action.payload
            }

        case(types.postUserData):
            return {
                ...state,
                userData: action.payload
            }

        default:
            return state
    }
}

export default userTokensReducer