import types from './types'

const postUserToken = (userToken) => {
    return {
        type: types.postUserToken,
        payload: userToken
    }
}

const postRefreshToken = (refreshToken) => {
    return {
        type: types.postRefreshToken,
        payload: refreshToken
    }
}

const postUserData = (userData) => {
    return {
        type: types.postUserData,
        payload: userData
    }
}

const userDataActions = {
    postUserToken,
    postRefreshToken,
    postUserData
}

export default userDataActions