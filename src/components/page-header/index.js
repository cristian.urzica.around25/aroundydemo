import React, { Component } from 'react'
import { Text, ImageBackground } from 'react-native'

import styles from './style'
import { images } from '../../assets'

export default class Header extends Component {
  render() {
    return (
      <ImageBackground style={styles.thumbnail} source={images.thumbnail}>
        <Text style={styles.headerTitle}>{this.props.title}</Text>
      </ImageBackground>
    )
  }
}
