import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
    thumbnail: {
        overflow: 'hidden',
        width: width,
        height: height / 6,
        borderBottomLeftRadius: 60,
        borderBottomRightRadius: 60,
        justifyContent: 'center',
      },
      headerTitle: {
        fontSize: 28,
        color: 'white',
        marginHorizontal: 25
      }
})

export default styles