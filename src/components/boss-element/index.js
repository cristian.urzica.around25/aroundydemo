import React, { Component } from 'react'
import { Text, View, Switch, Image, TouchableOpacity } from 'react-native'
import styles from './style';
import { icons } from '../../assets'

export default class Boss extends Component {

  constructor(props) {
    console.disableYellowBox = true;
    super(props);
    this.state = {
      checked: false
    };
    this.onValueChangeHandler = this.onValueChangeHandler.bind(this)
    this.onCheckEventHandler = this.onCheckEventHandler.bind(this)
  }

  onValueChangeHandler(){
    this.setState({
      checked: !this.state.checked
    })
    this.props.callback(this.props.index)
  }

  onCheckEventHandler(){
    this.setState({
      ...this.state,
      checked: !this.state.checked
    })
    this.props.callback(this.props.index)
  }

  render() {
    return (
      <TouchableOpacity style={styles.base} key={this.props.name} onPress={this.onCheckEventHandler}>
        <Image source={this.state.checked ? icons.checkmark : icons.checkmarkus}/>
          <Text style={styles.text}>{this.props.name}</Text>
      </TouchableOpacity>
    )
  }
}
