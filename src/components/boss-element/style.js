import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
  base: {
    flexDirection:'row',
    marginVertical: 8
  },
  text:{
    fontSize: 18,
    marginStart: 8
  }
})

export default styles