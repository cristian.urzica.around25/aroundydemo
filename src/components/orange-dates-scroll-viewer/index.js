import React, { Component } from 'react'
import { Text, ScrollView } from 'react-native'

import styles from './style'

const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

export default class OrangeDates extends Component {

  constructor(props) {
    console.disableYellowBox = true;
    super(props);
    this.getHolidayPeriodsComponents = this.getHolidayPeriodsComponents.bind(this)
  }

  getHolidayPeriodsComponents(intervalsDays, singleDays) {
    if(!intervalsDays && !singleDays) return null
    let dates = intervalsDays.map((element, index) => {
      if (index % 2 === 1) return null;

      const startDate = new Date(element)
      const endDate = new Date(intervalsDays[index + 1])
      const message = startDate.getDate() + " " + months[startDate.getMonth()] + " " + startDate.getFullYear() + ' to '
        + endDate.getDate() + " " + months[endDate.getMonth()] + " " + endDate.getFullYear()
      return (
        <Text key={100+index} style={styles.orangeText}>{message}</Text>
      )
    })
    dates = [...dates, singleDays.map((element, index) => {
      const date = new Date(element)
      const message = date.getDate() + " " + months[date.getMonth()] + " " + date.getFullYear()
      return (
        <Text key={1000+index} style={styles.orangeText}>{message}</Text>  
      )
    })]
    return dates
  }

  render() {
    return (
      <ScrollView
        style={[this.props.style]}
        showsVerticalScrollIndicator={false}>
        {this.getHolidayPeriodsComponents(this.props.intervalsDays, this.props.singleDays)}
      </ScrollView>
    )
  }
}
