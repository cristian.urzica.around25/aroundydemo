import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({

    orangeText:{
        fontSize: 18,
        color: '#FF9123',
        textAlign: 'center'
    }
})

export default styles