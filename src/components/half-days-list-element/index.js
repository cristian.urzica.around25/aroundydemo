import React, { Component } from 'react'
import { Text, View, Switch } from 'react-native'

import { formateDateDayMonth } from '../../utility'
import styles from './style'

export default class HalfComponent extends Component {

  constructor(props) {
    super(props);
    this.state={
      switchValue: false
    }
    this.onStateChangeEventHandler = this.onStateChangeEventHandler.bind(this)
  }

  onStateChangeEventHandler(){
    this.setState({
      ...this.state,
      switchValue: !this.state.switchValue})
    this.props.callback(this.props.index)
  }

  render() {
    return (
      <View key={date.index} style={styles.base}>
        <Text style={styles.text}>{formateDateDayMonth(this.props.date.item)}</Text>
        <Switch
          style={styles.switch}
          onValueChange={this.onStateChangeEventHandler}
          value={this.state.switchValue}
          trackColor={{false:'grey', true:'#FF9123'}} />
      </View>
    )
  }
}
