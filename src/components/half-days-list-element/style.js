import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
  base: {
    flexDirection: 'row',
    width: '100%',
    alignSelf: 'center',
    justifyContent: 'space-between',
    marginVertical: 4
  },
  text:{
    fontSize: 22,
    color: '#646464'
  },
  switch:{
    transform: [{ scaleX: 1.3 }, { scaleY: 1.3 }],
    marginRight: 12
  }
})

export default styles