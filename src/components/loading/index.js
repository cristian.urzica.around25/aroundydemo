import React, { Component } from 'react'
import { Text, View, ActivityIndicator, Image } from 'react-native'
import LottieView from 'lottie-react-native';

import { images } from '../../assets'

export default class Loading extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <LottieView source={require('../../assets/animation/pong.json')} autoPlay={true} loop={true} />
      </View>
    )
  }
}
