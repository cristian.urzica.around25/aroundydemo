import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

import styles from './style'
import { TextInput } from 'react-native-gesture-handler';

export default class CustomUserInput extends Component {

	constructor(props) {
		console.disableYellowBox = true;
		super(props);
		this.state = {
		};
	}

	render() {
		return (
			<View style={[styles.container, this.props.style]}>

				<View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 8 }}>
					<Image
						source={this.props.icon} />
					<TextInput
						placeholder={this.props.placeholder}
						secureTextEntry={this.props.isPassword}
						style={styles.inputText}
						placeholderTextColor={'white'}
						onChangeText={(text) => this.props.onTextChange(text)}
					/>
				</View>
				<LinearGradient
					colors={['#FAD961', '#E77A39']}
					style={styles.underline} />

			</View>
		)
	}
}
