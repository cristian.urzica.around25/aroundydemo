import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
    container: {
        width: width - 64,
        marginTop: height/13
    },
    underline: {
        height: 1,
        width: '100%'
    },
    inputText: {
        width: '100%',
        fontSize: 18,
        color: 'white',
        paddingBottom: 4,
        marginStart: 8
    },
    icon:{
        width: 24,
        height: 24
    }
})

export default styles