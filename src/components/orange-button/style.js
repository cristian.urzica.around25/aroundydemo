import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
  text: {
    fontSize: 18,
    color: 'white'
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20
  }
})

export default styles