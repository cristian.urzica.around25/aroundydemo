import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import styles from './style'

export default class GradientButton extends Component {

  constructor(props) {
    console.disableYellowBox = true;
    super(props);
    this.state = {
    };
  }

  render() {
    if (this.props.transparent) {
      return (
        <TouchableOpacity
          style={[styles.container, this.props.style]}
          onPress={this.props.onPress}>
          <View
            style={[styles.container, this.props.style]}>
            <Text style={[styles.text, {color:'#E77A39'}]}>{this.props.text}</Text>
          </View>
        </TouchableOpacity>
      )
    }
    return (
      <TouchableOpacity
        style={[styles.container, this.props.style]}
        onPress={this.props.onPress}>
        <LinearGradient
          style={[styles.container, this.props.style]}
          colors={['#F84100', '#F9D213']}>
          <Text style={styles.text}>{this.props.text}</Text>
        </LinearGradient>
      </TouchableOpacity>
    )
  }
}
