import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import OrangeButton from '../orange-button'

import styles from './style'

export default class NextBackModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    if (!this.props.isOrange)
      return (
        <View style={[styles.base, this.props.style]}>
          <TouchableOpacity
            style={styles.buttons}
            onPress={this.props.onFirstButtonPress}>
            <Text style={styles.textButton}>{this.props.firstButtonText}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.buttons}
            onPress={this.props.onSecondButtonPress}>
            <Text style={styles.textButton}>{this.props.secondButtonText}</Text>
          </TouchableOpacity>
        </View>
      );
    return (
      <View style={[styles.base, this.props.style]}>
        <TouchableOpacity
          style={styles.buttons}
          onPress={this.props.onFirstButtonPress}>
          <Text style={styles.textButton}>{this.props.firstButtonText}</Text>
        </TouchableOpacity>
        <OrangeButton
          style={styles.buttons}
          text={this.props.secondButtonText}
          onPress={this.props.onSecondButtonPress} />
      </View>
    )
  }
}
