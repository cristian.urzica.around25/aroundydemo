import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
  base: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  buttons: {
    height: 55,
    width: width/2.6,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    elevation: 10,
    marginRight: 5,
    borderRadius: 50
  },
  textButton:{
    fontSize: 18
  }
})

export default styles