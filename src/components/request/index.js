import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'
import styles from './style';

import { images } from '../../assets'
import OrangeButton from '../../components/orange-button'

export default class Request extends Component {
  render() {
    return (
      <View style={styles.base}>
        <Image 
        source={images.profilePic}
        style={styles.profilePic}/>
        <View style={styles.descriptionContainer}>
          <Text style={styles.normalText}>{this.props.name}</Text>
          <Text style={styles.normalText}>Vacation: {this.props.from.slice(0,10)} to {this.props.to.slice(0,10)}</Text>
          <Text style={styles.smallText}>Time ago</Text>
        </View>
        <View 
        style={styles.buttonsContainer}
        key={this.props.index}>
          <OrangeButton 
          text={'APPROVE'}
          style={styles.button}
          onPress={() => this.props.callback('aprove', this.props.index)}/>
          <OrangeButton 
          text={'REJECT'}
          style={styles.button}
          transparent={true}
          onPress={() => this.props.callback('not-approved', this.props.index)}/>
        </View>
      </View>
    )
  }
}
