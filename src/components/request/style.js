import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
    base:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'space-between',
        paddingVertical: 8,
        borderWidth: 1,
        borderColor: '#DFDFDF',
        paddingHorizontal:'4%',
        height: height/5

    },
    profilePic:{
      borderRadius: 100,
      width: 94,
      height: 94
    },
    descriptionContainer:{
      justifyContent:'space-between',
      marginStart: 4,
      height: '85%',
      width: '28%',
    },
    buttonsContainer:{
      justifyContent:'space-between',
      marginStart: 10,
      height: '80%',
      marginRight: 25,
      width: 120
    },
    normalText:{
      fontSize: 14
    },
    smallText:{
      fontSize: 11,
      color: '#646464'
    },
    button:{
      height: 40
    }
})

export default styles