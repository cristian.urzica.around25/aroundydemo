import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'

import styles from './style'

export default class IconTextValue extends Component {
  render() {
    return (
      <View style={styles.balanceContainer}>
        <View style={{ width: '50%', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
          <View style={{ width: 30, justifyContent: 'center', alignItems: 'flex-start'}}>
            <Image source={this.props.icon} />
          </View>
          <Text style={styles.normalText}>{this.props.description}</Text>
        </View>
        <Text style={styles.valueText}>{this.props.value} Days</Text>
      </View>
    )
  }
}
