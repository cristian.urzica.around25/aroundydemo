import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

const height = Dimensions.get('window').height
const width = Dimensions.get('window').width

const styles = StyleSheet.create({
    balanceContainer:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginVertical: 8
    },
    normalText:{
        fontSize: 20,
        
    },
    valueText:{
        fontSize: 28,
        width: '50%',
        textAlign: 'center',
        flex:1
    }
})

export default styles