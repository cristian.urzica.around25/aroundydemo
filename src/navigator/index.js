import React from 'react';
import baseNavigator from './navigators/baseNavigator';
import { Text, View } from 'react-native';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';


const BaseAppContainer = createAppContainer(baseNavigator)

export default BaseAppContainer