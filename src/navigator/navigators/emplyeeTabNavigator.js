import React, { Component } from 'react'
import { createBottomTabNavigator } from 'react-navigation';
import { employeeTabRoots, employeeTabSettings } from '../roots/employeeTabRoots'

const Employee = createBottomTabNavigator(
  employeeTabRoots,
  {
    tabBarOptions:{
      activeTintColor: 'orange',
      inactiveTintColor: 'grey',
      showLabel: false,
      style:{
        borderRadius: 54,
        height: 70,
        elevation: 5,
      }
    }
  }
)

export default Employee