import { createStackNavigator } from 'react-navigation';
import { flyAwayRoot, flyAwayRootConfig  } from '../roots/flyAwayRoot'

const flyAwayNavigator = createStackNavigator(
    flyAwayRoot, flyAwayRootConfig 
)

export default flyAwayNavigator