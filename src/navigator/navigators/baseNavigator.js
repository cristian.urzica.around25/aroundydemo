import { createStackNavigator, createSwitchNavigator, createAppContainer } from 'react-navigation';
import { baseRoot, baseRootConfig  } from '../roots'

const baseNavigator = createSwitchNavigator(
    baseRoot, baseRootConfig 
)

export default baseNavigator