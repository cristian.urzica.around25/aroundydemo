import { createStackNavigator } from 'react-navigation'
import { adminRoot, adminRootConfig } from '../roots/adminRoot'

const Admin = createStackNavigator(
    adminRoot, adminRootConfig
)

export default Admin