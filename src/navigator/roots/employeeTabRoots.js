import Balance from '../../modules/screens/emplyee-screens/balance/redux-container'
import flyAwayNavigator from '../navigators/flyAwayNavigator'
import Profile from '../../modules/screens/profile'
import { icons } from '../../assets'
import React, { Component } from 'react'
import { Image } from 'react-native'

const employeeTabRoots = {
  Balance: {
    screen: Balance,
    navigationOptions:{
      tabBarIcon:({tintColor}) => (
        <Image style={{width: 24, height: 24}} source={tintColor === 'orange' ? icons.europeActive : icons.europeInactive}/>
      )
    }
  },
  FlyAway: {
    screen: flyAwayNavigator,
    navigationOptions:{
      tabBarIcon:({tintColor}) => (
        <Image style={{width: 24, height: 24}} source={tintColor === 'orange' ? icons.rocketActive : icons.rocketInactive}/>
      )
    }
  },
  Profile: {
    screen: Profile,
    navigationOptions:{
      tabBarIcon:({tintColor}) => (
        <Image style={{width: 24, height: 24}} source={tintColor === 'orange' ? icons.helmActive : icons.helmInactive}/>
      )
    }
  },
  // Register: Register
}

const employeeTabSettings = {
  initialRouteName: 'FlyAway'
}

export { employeeTabRoots, employeeTabSettings }