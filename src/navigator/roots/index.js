import Login from '../../modules/screens/login/redux-container'
import Register from '../../modules/screens/register'
import Employee from '../navigators/emplyeeTabNavigator'
import Admin from '../navigators/adminNavigator'

const baseRoot = {
    Login: Login,
    Register: Register,
    Employee: Employee,
    Admin: Admin
    // Balance: Balance
}

const baseRootConfig = {
    initialRouteName: 'Login'
}

export { baseRoot, baseRootConfig }