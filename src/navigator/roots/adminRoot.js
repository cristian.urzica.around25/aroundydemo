import AdminAproval from '../../modules/screens/admin-screens/admin-aproval/redux-container'

const adminRoot = {
  AdminAproval
}

const adminRootConfig = {
  initialRouteName: 'AdminAproval',
  headerMode: 'none',
  navigationOptions: {
      headerVisible: false,
  }
}

export {
  adminRoot,
  adminRootConfig
}
