import FlyAway from '../../modules/screens/fly-away/calendar-screen/redux-container'
import LeaveType from '../../modules/screens/fly-away/leave-type/redux-container'
import HalfDays from '../../modules/screens/fly-away/half-days/redux-container'
import YourBalance from '../../modules/screens/fly-away/your-balance/redux-container'
import SendBosses from '../../modules/screens/fly-away/send-bosses/redux-container'

const flyAwayRoot = {
    FlyAwayCalendar: FlyAway,
    LeaveType: LeaveType,
    HalfDays: HalfDays,
    YourBalance,
    SendBosses
}

const flyAwayRootConfig = {
    initialRouteName: 'FlyAwayCalendar',
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false,
    }
}

export { flyAwayRoot, flyAwayRootConfig }